﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SalaryCalc.Models;
using SC.Core.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SalaryCalc.Controllers
{
    public class HomeController : Controller
    {
        public static List<GenericWorker> workList = null;
        public IActionResult Index()
        {
            CreateTestData();
            return View(workList);
        }
        public bool CreateNewSubordinate(string name, DateTime date, int typeId, string guid)
        {
            GenericWorker parent = GetGenericWorkerById(workList, guid);

            GenericWorker workerToAdd = GetWorkerByType(typeId);
            workerToAdd.Name = name;
            workerToAdd.EnrollmentDate = date;
            workerToAdd.Chief = parent;

            if (parent == null)
            {
                workList.Add(workerToAdd);
            }

            if (parent is ITopManagment)
            {
                if ((parent as ITopManagment).Subordinates == null)
                {
                    (parent as ITopManagment).Subordinates = new List<GenericWorker>();
                }

                (parent as ITopManagment).Subordinates.Add(workerToAdd);
            }

            return true;
        }
        public double CalculateSalary(DateTime date, string guid)
        {
            GenericWorker worker = GetGenericWorkerById(workList, guid);
            return worker.CalculateSalary(date);
        }
        public double CalculateCompanySalary(DateTime date)
        {
            double total = 0;

            foreach (GenericWorker worker in workList)
            {
                total += worker.CalculateSalary(date);
            }

            return total;
        }
        

        private GenericWorker GetWorkerByType(int typeId)
        {
            switch (typeId)
            {
                case 0:
                    return new Emploeyee();
                case 1:
                    return new Manager() { Subordinates = new List<GenericWorker>(), };
                case 2:
                    return new Sales() { Subordinates = new List<GenericWorker>() };
                default:
                    return new Emploeyee();
            }
        }
        private GenericWorker GetGenericWorkerById(IList<GenericWorker> list, string guid)
        {
            GenericWorker retWorker = null;

            foreach (GenericWorker worker in list)
            {
                if (worker.Id.Equals(guid))
                    return worker;

                if (worker is ITopManagment)
                {
                    retWorker = GetGenericWorkerById((worker as ITopManagment).Subordinates, guid);
                    if (retWorker != null)
                        break;
                }
            }

            return retWorker;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private void CreateTestData()
        {
            if (workList == null)
            {
                Sales sales = new Sales() { Name = "John", EnrollmentDate = new DateTime(2000, 01, 01) };
                Manager manager = new Manager() { Name = "Bill", EnrollmentDate = new DateTime(2000, 01, 01) };
                Sales sales2 = new Sales() { Name = "Kevin", EnrollmentDate = new DateTime(2000, 01, 01) };

                
                sales.Subordinates.Add(manager);
                manager.Subordinates.Add(sales2);
                sales2.Subordinates.Add(new Emploeyee() { Name = "Antony", EnrollmentDate = new DateTime(2000, 01, 01) });

                //manager.Subordinates = new List<GenericWorker>();
                //sales.Subordinates = new List<GenericWorker>();

                //manager.EnrollmentDate = new DateTime(2000, 01, 01);
                //sales.EnrollmentDate = 

                //manager.Subordinates.Add(new Emploeyee() { Name = "Kevin", EnrollmentDate = new DateTime(2000, 01, 01) });
                //manager.Subordinates.Add(new Emploeyee() { Name = "Martin", EnrollmentDate = new DateTime(2000, 01, 01) });
                //manager.Subordinates.Add(new Emploeyee() { Name = "Antony", EnrollmentDate = new DateTime(2000, 01, 01) });

                //sales.Subordinates.Add(manager);

                workList = new List<GenericWorker>();
                workList.Add(sales);
            }
        }

    }
}
