using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalaryCalc.Controllers;
using SC.Core.Domain;
using System;
using System.Collections.Generic;

namespace SC.SalaryCalc.Test
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            /*
         0   sales         baseSalary0 + (10050.45 + 10090.24+10050.15+10030+10000) * 0.3% = 10150,66252
               |
         1   manager       baseSalary1 + (salary2 * bonusRate) = 10000 + (10090.24045)*0.5% = 10050.45120225
               |
         2   sales         baseSalary2 + ((salary3 + salary4 + salary5) * bonusRate) = 10000 + (10050.15 + 10030 + 10000) * 0.3% = 10090.24045
               |
         3   manager       baseSalary3 + (salary4 * bonusRate) = 10000 + (10030)*0.5% = 10050.15
               |
         4   sales         baseSalary4 + (salary5 * bonusRate) = 10000 + (10000*0.3%) = 10030
               |
         5  employee       baseSalary5 = 10000

             */

            HomeController controller = new HomeController();

            HomeController.workList = new List<GenericWorker>();

            Sales sales0 = new Sales() { EnrollmentDate = new DateTime(2000, 01, 01) };
            Manager mngr1 = new Manager() { EnrollmentDate = new DateTime(2000, 01, 01) };
            Sales sales2 = new Sales() { EnrollmentDate = new DateTime(2000, 01, 01) };
            Manager mngr3 = new Manager() { EnrollmentDate = new DateTime(2000, 01, 01) };
            Sales sales4 = new Sales() { EnrollmentDate = new DateTime(2000, 01, 01) };
            Emploeyee emp5 = new Emploeyee() { EnrollmentDate = new DateTime(2000, 01, 01) };


            sales0.Subordinates.Add(mngr1);
            mngr1.Subordinates.Add(sales2);
            sales2.Subordinates.Add(mngr3);
            mngr3.Subordinates.Add(sales4);
            sales4.Subordinates.Add(emp5);

            HomeController.workList.Add(sales0);
            
            Assert.AreEqual(10150.66252, Math.Round(controller.CalculateCompanySalary(new DateTime(2000,11, 01)),5));
        }
    }
}
