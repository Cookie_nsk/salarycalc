﻿using System;
using System.Collections.Generic;

namespace SC.Core.Domain
{
    public class Sales : GenericWorker, ITopManagment
    {
        public IList<GenericWorker> Subordinates { get; set; }
        public double SubordinatesSalaryBonus { get { return 0.003; } }
        public override double YearBonusRate { get { return 0.01; } }
        public override double MaxBonusRate { get { return 0.35; } }

        public Sales()
        {
            Subordinates = new List<GenericWorker>();
        }
        public override double CalculateSalary(DateTime onDate)
        {//это базовая ставка плюс 1% за каждый год работы в компании (но не больше 35% суммарной надбавки за стаж работы) плюс 0,3% зарплаты всех подчинённых всех уровней.
            double subordinatesSalary = 0;

            GetAllSubordinatesSalary(Subordinates, ref subordinatesSalary, onDate);

            return GetBaseSalary(onDate) + (subordinatesSalary * SubordinatesSalaryBonus);
        }
        private void GetAllSubordinatesSalary(IList<GenericWorker> subordinates, ref double salary, DateTime onDate)
        {
            foreach (GenericWorker worker in subordinates)
            {
                if (worker is Manager)
                {
                    salary += (worker as Manager).CalculateSalary(onDate);
                }
                else if (worker is Sales)
                {
                    salary += (worker as Sales).CalculateSalary(onDate);
                }
                else
                {
                    salary += worker.CalculateSalary(onDate);
                }

                if (worker is ITopManagment)
                {
                    GetAllSubordinatesSalary((worker as ITopManagment).Subordinates, ref salary, onDate);
                }
            }
        }
    }
}
