﻿using System;

namespace SC.Core.Domain
{
    public class Emploeyee : GenericWorker
    {
        public override double YearBonusRate { get { return 0.03; } }
        public override double MaxBonusRate { get { return 0.3; } }
        public override double CalculateSalary(DateTime onDate)
        {//это базовая ставка плюс 3% за каждый год работы в компании, но не больше 30% суммарной надбавки.
            return GetBaseSalary(onDate);
        }
    }
}
