﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace SC.Core.Domain
{
    public class Manager : GenericWorker, ITopManagment
    {
        public IList<GenericWorker> Subordinates { get; set; }
        public double SubordinatesSalaryBonus { get { return 0.005; } }
        public override double YearBonusRate { get { return 0.05; } }
        public override double MaxBonusRate { get { return 0.4; } }

        public Manager()
        {
            Subordinates = new List<GenericWorker>();
        }
        public override double CalculateSalary(DateTime onDate)
        {//это базовая ставка плюс 5% за каждый год работы в компании (но не больше 40% суммарной надбавки за стаж работы) плюс 0,5% зарплаты всех подчинённых первого уровня
            double subordinatesSalary = 0;

            foreach (GenericWorker worker in Subordinates)
            {
                if (worker is Manager)
                {
                    subordinatesSalary += (worker as Manager).CalculateSalary(onDate);
                }
                else if (worker is Sales)
                {
                    subordinatesSalary += (worker as Sales).CalculateSalary(onDate);
                }
                else
                {
                    subordinatesSalary += worker.CalculateSalary(onDate);
                }
            }

            return GetBaseSalary(onDate) + (subordinatesSalary * SubordinatesSalaryBonus);
        }
    }
}
