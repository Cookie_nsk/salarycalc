﻿using System;

namespace SC.Core.Domain
{
    public abstract class GenericWorker
    {
        public string Id { get; private set; }
        public string Name { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public double BaseRate { get { return 10000; } }
        public GenericWorker Chief { get; set; }
        public abstract double YearBonusRate { get; }
        public abstract double MaxBonusRate { get; }

        public GenericWorker()
        {
            Id = Guid.NewGuid().ToString();
        }

        public abstract double CalculateSalary(DateTime onDate);
        internal double GetBaseSalary(DateTime onDate)
        {//если работает 11 лет то ставка не 33, а 30

            if (onDate < EnrollmentDate)
                return 0;

            int totalYearsWorked = onDate.Year - EnrollmentDate.Year;

            if (totalYearsWorked == 0)
                return BaseRate;

            if (totalYearsWorked * YearBonusRate < MaxBonusRate)
            {
                return BaseRate + (BaseRate * totalYearsWorked * YearBonusRate);
            }
            else
            {
                return BaseRate + (BaseRate * MaxBonusRate);
            }
        }
    }
}
