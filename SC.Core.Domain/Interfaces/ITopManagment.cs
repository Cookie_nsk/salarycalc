﻿using System;
using System.Collections.Generic;

namespace SC.Core.Domain
{
    public interface ITopManagment 
    {
        double SubordinatesSalaryBonus { get; }
        IList<GenericWorker> Subordinates { get; set; }
    }
}
