using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SC.Core.Domain.Test
{
    [TestClass]
    public class ClassLogicTests
    {
        [TestMethod]
        public void EmployeeCalculateSalaryTest()
        {//Base rate 10000
            Emploeyee emp = new Emploeyee() { EnrollmentDate = new DateTime(2000, 01, 01) };
            
            //0
            Assert.AreEqual(0, emp.CalculateSalary(new DateTime(1999, 12, 01)));
            //0%
            Assert.AreEqual(10000, emp.CalculateSalary(new DateTime(2000, 11, 01)));
            //3%
            Assert.AreEqual(10300, emp.CalculateSalary(new DateTime(2001, 12, 01)));
            //MaxBonus rate
            Assert.AreEqual(13000, emp.CalculateSalary(new DateTime(2010, 12, 01)));
        }

        [TestMethod]
        public void ManagerCalculateSalaryTest()
        {//Base rate 10000
            Manager mngr = new Manager()
            {
                EnrollmentDate = new DateTime(2000, 01, 01)
            };

            //0
            Assert.AreEqual(0, mngr.CalculateSalary(new DateTime(1999, 12, 01)));
            //0%
            Assert.AreEqual(10000, mngr.CalculateSalary(new DateTime(2000, 11, 01)));
            //5% + 0% workers
            Assert.AreEqual(10500, mngr.CalculateSalary(new DateTime(2001, 12, 01)));
            //MaxBonus rate
            Assert.AreEqual(14000, mngr.CalculateSalary(new DateTime(2010, 12, 01)));
            /*
            0       Manager           = 10000 + 10060*0.5% = 10050.3
                       |
            1        sales            = 10000 + (20000*0.3%) = 10060
                  |          |
            2   Emploeyee Emploeyee   = 20000
        
            */
            //Calculate salary only for the first level of workers 0,5% of all = 10000 * 0.5% = 50
            Emploeyee emp0 = new Emploeyee() { EnrollmentDate = new DateTime(2000, 01, 01) };
            Emploeyee emp1 = new Emploeyee() { EnrollmentDate = new DateTime(2000, 01, 01) };
            
            Sales sls = new Sales() { EnrollmentDate = new DateTime(2000, 01, 01) };
            sls.Subordinates.Add(emp0);
            sls.Subordinates.Add(emp1);

            mngr.Subordinates.Add(sls);

            Assert.AreEqual(10050.3, mngr.CalculateSalary(new DateTime(2000, 11, 01)));
        }

        [TestMethod]
        public void SalesCalculateSalaryTest()
        {//Base rate 10000
            /*
         0   sales         baseSalary0 + (salary1+salary2+salary3) * bonusRate
               |
         1   manager       baseSalary1 + (salary2 * bonusRate)
               |
         2   sales         baseSalary2 + (salary3 * bonusRate)
               |
         3   employee      baseSalary3

                                          10050.15                             10030                 10000 
            total = 10000 + (s1(10000 + (10000 + 10000*0.03) * 0.5%) + s2(10000 + 10000 * 0.3%) + s3(10000)) * 0.3% = 10090.24
             */


            Sales sales = new Sales()
            {
                EnrollmentDate = new DateTime(2000, 01, 01)
            };

            //0
            Assert.AreEqual(0, sales.CalculateSalary(new DateTime(1999, 12, 01)));
            //0%
            Assert.AreEqual(10000, sales.CalculateSalary(new DateTime(2000, 11, 01)));
            //1% + 0% workers
            Assert.AreEqual(10100, sales.CalculateSalary(new DateTime(2001, 12, 01)));
            //MaxBonus rate
            Assert.AreEqual(13500, sales.CalculateSalary(new DateTime(2050, 12, 01)));

            Manager mngr = new Manager() { EnrollmentDate = new DateTime(2000, 01, 01) };

            Emploeyee emp0 = new Emploeyee() { EnrollmentDate = new DateTime(2000, 01, 01) };

            Sales sales0 = new Sales() { Name="subSale", EnrollmentDate = new DateTime(2000, 01, 01) };
            sales0.Subordinates = new List<GenericWorker>();
            sales0.Subordinates.Add(emp0);

            mngr.Subordinates.Add(sales0);

            sales.Subordinates.Add(mngr);

            Assert.AreEqual(10090,24, sales.CalculateSalary(new DateTime(2000, 11, 01)));
        }
    }
}
